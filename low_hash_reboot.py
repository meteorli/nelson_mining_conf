import os
import sys
import time 


ethos_stats_file = "/var/run/ethos/stats.file"
min_hash_speed = 10
min_up_time = 1200
log_file = "/home/ethos/rig_reboot_log.txt"

if os.path.isfile( ethos_stats_file):
    f = open(ethos_stats_file)
    stats_lines = f.readlines()
    f.close()

hash_speed_list = []
uptime = 0
for line in stats_lines:
    if line.startswith("miner_hashes:"):
        #print line
        hash_speed_string = line.replace("miner_hashes:", "")
        hash_speed_list = hash_speed_string.strip().split(" ")

    if line.startswith("uptime:"):
        uptime_string = line.replace("uptime:", "").strip()
        uptime = float(uptime_string)

# check the boot time
if uptime >= min_up_time:
    miner_no = 0
    for hash_speed in hash_speed_list:
        if float(hash_speed) <= min_hash_speed:
            localtime = time.asctime( time.localtime(time.time()) )
            log_message = "%s: miner[%s] speed = %s, reboot" % (str(localtime), str(miner_no), str(hash_speed))
            #print log_message
            log_command = 'echo "%s" >> %s' % (log_message, log_file)
            print log_command
            os.system(log_command)
            os.system('/opt/ethos/bin/r')

        miner_no = miner_no + 1


